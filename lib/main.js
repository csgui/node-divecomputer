'use strict';

var libDiveComputer = require('./libdivecomputerffi');

/**
 * Entry point to libdivecomputer features
 */
var callDeviceFunction = function (parameters, callback) {
  var options = parameters.options;
  var params = parameters.params;
  var loglevel = options.loglevel || 0;   // TODO change this O value to enum...

  var context = setContext(loglevel, callback);
  var device = setDevice(context, options, callback);
  console.log(device);
  var x = setEvents(device);

  if(device != null) {
    return deviceDives(params);
  }
};

var setContext = function(loglevel, callback) {
  var context = libDiveComputer.context(loglevel);

  // if(status != 0) {
  //   callback(new Error("device context set has failed"));
  //   return null;
  // } else {
  //   callback(null, "device context properly set");
  // }

  return context;
};

var setDevice = function(context, options, callback) {
  var port = options.port;
  var model = options.model;
  var device = libDiveComputer.device(context, model, port);

  // if(status != 0) {
  //   callback(new Error("device set has failed"));
  //   return null;
  // } else {
  //   callback(null, "device properly set");
  // }

  return device;
};

var setEvents = function(device) {
  libDiveComputer.setEvents(device);
};

var deviceDives = function (params) {
  console.log("---| Calling deviceDives function |---");
};

module.exports = callDeviceFunction;
