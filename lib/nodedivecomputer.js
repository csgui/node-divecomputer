'use strict';

/**
 * Load the devices from devices/index.js file
 * @private
 */
var devices = require('../devices');

/**
 * NodeDiveComputer constructor.
 * @constructor
 */
function NodeDiveComputer() {
  this.addDevices(devices);
}

/**
 * Add Devices to nodedivecomputer object
 * E.g. nodedivecomputer.scubapro and nodedivecomputer.mares
 *
 * @param {Array} devices Devices to be added
 * @private
 */
NodeDiveComputer.prototype.addDevices = function(devices) {
  for(var deviceName in devices) {
    this[deviceName] = devices[deviceName].bind(this);
  }
};

NodeDiveComputer.prototype.model = {
  SCUBAPRO_ALADIN: 'uwatec_aladin',
  HW_OSTC3: 'hw_ostc3'
};

var nodedivecomputer = new NodeDiveComputer();

/**
 * Exports nodedivecomputer object
 */
module.exports = nodedivecomputer;
