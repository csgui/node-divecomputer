'use strict';

var ffi = require('ffi');
var ref = require('ref');
var StructType = require('ref-struct');

// struct dc_context_t {
//          dc_loglevel_t loglevel;
//          dc_logfunc_t logfunc;
//          void *userdata;
// #ifdef ENABLE_LOGGING
//          char msg[8192 + 32];
// #ifdef _WIN32
//          LARGE_INTEGER timestamp, frequency;
// #else
//          struct timeval timestamp;
// #endif
// #endif
// };
var dcContextT = StructType();
var dcContextTPtr = ref.refType(dcContextT);
var dcContextTPtrPtr = ref.refType(dcContextTPtr);
dcContextT.defineProperty('loglevel', 'int');
// typedef void (*dc_logfunc_t) (dc_context_t *context, dc_loglevel_t loglevel, const char *file, unsigned int line, const char *function, const char *message, void *userdata);
var logFunctionPtr = ffi.Function( 'void', [ dcContextTPtr, 'int', 'string', 'int', 'string', 'string', ref.refType(ref.types.void) ]);
dcContextT.defineProperty('logfunc', logFunctionPtr);
dcContextT.defineProperty('userdata', ref.refType(ref.types.void));

// struct dc_device_t {
//          const dc_device_vtable_t *vtable;
//          // Library context.
//          dc_context_t *context;
//          // Event notifications.
//          unsigned int event_mask;
//          dc_event_callback_t event_callback;
//          void *event_userdata;
//          // Cancellation support.
//          dc_cancel_callback_t cancel_callback;
//          void *cancel_userdata;
//          // Cached events for the parsers.
//          dc_event_devinfo_t devinfo;
//          dc_event_clock_t clock;
// };
var dcDeviceT = StructType();
var dcDeviceTPtr = ref.refType(dcDeviceT);
var dcDeviceTPtrPtr = ref.refType(dcDeviceTPtr);
var callbackFunctionPtr = ffi.Function( 'void', [ dcDeviceTPtr, 'int', ref.refType(ref.types.void), ref.refType(ref.types.void) ]);
dcDeviceT.defineProperty('event_callback', callbackFunctionPtr);

var libdivecomputer = ffi.Library('libdivecomputer', {
  // dc_status_t dc_context_new (dc_context_t **context);
  'dc_context_new': [ 'int', [ dcContextTPtrPtr ] ],

  // dc_status_t dc_context_set_loglevel (dc_context_t *context, dc_loglevel_t loglevel);
  'dc_context_set_loglevel': [ 'int', [ dcContextTPtr, 'int' ] ],

  // dc_status_t dc_context_set_logfunc (dc_context_t *context, dc_logfunc_t logfunc, void *userdata);
  'dc_context_set_logfunc': [ 'int', [ dcContextTPtr, logFunctionPtr, ref.refType(ref.types.void)] ],

  // dc_status_t dc_device_set_events (dc_device_t *device, unsigned int events, dc_event_callback_t callback, void *userdata);
  'dc_device_set_events': [ 'int', [ dcDeviceTPtr, 'int', callbackFunctionPtr, ref.refType(ref.types.void)] ],

  // dc_status_t hw_ostc3_device_open (dc_device_t **device, dc_context_t *context, const char *name);
  'hw_ostc3_device_open': [ 'int', [ dcDeviceTPtrPtr, dcContextTPtr, 'string' ] ],

  // dc_status_t uwatec_aladin_device_open (dc_device_t **out, dc_context_t *context, const char *name)
  'uwatec_aladin_device_open': [ 'int', [ dcDeviceTPtrPtr, dcContextTPtr, 'string' ] ]
});

// intentionally blank
function LibDiveComputerFFI() {}

LibDiveComputerFFI.prototype.context = function(loglevel) {
  var logFunction = function(context, loglevel, file, line, fn, message, userData) {
    console.log(file, line, fn, message, userData);
  };
  var context = new dcContextT();

  libdivecomputer.dc_context_new((context.ref()).ref());
  libdivecomputer.dc_context_set_loglevel(context.ref(), loglevel);
  libdivecomputer.dc_context_set_logfunc(context.ref(), logFunction, null);

  return context;
};

LibDiveComputerFFI.prototype.device = function(context, model, port) {
  var device = new dcDeviceT();

  libdivecomputer[model.concat('_device_open')]((device.ref()).ref(), context.ref(), port);

  return device;
};

LibDiveComputerFFI.prototype.setEvents = function(device) {
  var callbackFunction = function(device, event, data, userData) {
    console.log(device, event, data, userData);
  };
  console.log(libdivecomputer.dc_device_set_events(device.ref(), 3, callbackFunction, null));
};

var libDiveComputerFFI = new LibDiveComputerFFI();

module.exports = libDiveComputerFFI;
