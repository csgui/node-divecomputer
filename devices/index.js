'use strict';

var path = require('path');

/**
 * Return a Function that requires a Device from the disk
 * @param  {String} filename Filename of Device
 * @return {function} function used to require the Device from disk
 * @private
 */
function requireDevice(filename) {
  return function(options) {
    var devicePath = path.join(__dirname, filename);
    var Device = require(devicePath);
    var dev = new Device(options);

    return Object.freeze(dev);
  };
}

/**
 * Devices to be exported
 * @type {Object}
 * @private
 */
var devices = {
  'scubapro': requireDevice('scubapro')
};

/**
 * Exports the Devices
 * @type {Object}
 */
module.exports = devices;
