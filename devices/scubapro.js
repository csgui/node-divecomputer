'use strict';

var callDeviceFunction = require('../lib/main');

function Scubapro(options) {
  this.dives = {
    get: function(params, callback) {
      var parameters = {
        options: options,
        params: params
      };

      return callDeviceFunction(parameters, callback);
    }
  };
}

module.exports = Scubapro;
