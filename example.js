var divecomputer = require('./lib/nodedivecomputer');

// callback
var divesHandler = function(err, result) {
  if(err) {
    console.log('Error occurred: ', err);
  } else {
    console.log('Result: ', result);
  }
};

opts = {
  model: divecomputer.model.SCUBAPRO_ALADIN, // backend definition
  // model: divecomputer.model.HW_OSTC3,
  loglevel: 5,
  port: '/dev/tty.usbserial'
}
var aladin = divecomputer.scubapro(opts);

params = {};
aladin.dives.get(params, divesHandler);
